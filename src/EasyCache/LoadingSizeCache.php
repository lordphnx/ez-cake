<?php

namespace EZCake\EasyCache;


/**
 * @template K
 * @template V
 */
class LoadingSizeCache extends LoadingCache {

	private $max;
	private $current;

	public function __construct(callable $loader, int $max = 100) {
		parent::__construct($loader);
		$this->current = 0;
		$this->max = $max;
	}

	public function removeOne() {
		if (!empty($this->objects)) {
			$firstKey = \array_key_first($this->objects);
			unset($this->objects[$firstKey]);
			$this->current--;
		}
	}

	/**
	 * @param K $key
	 * @param V $value
	 */
	public function cache($key, $value) {
		parent::cache($key, $value);
		$this->current++;

		if ($this->current > $this->max) {
			$this->removeOne();
		}
	}

	/**
	 * Gets the current number of elements inside the cahce
	 * @return void
	 */
	public function getSize(): int {
//		return count($this->objects);
		return $this->current;
	}
}