<?php

namespace EZCake\EasyCache;

use Cake\Log\Log;
use Exception;

/**
 * @template K
 * @template V
 */
class LoadingCache extends Cache {


	private $loader;

	public function __construct(callable $loader) {
		parent::__construct();
		$this->loader = $loader;
	}


	/**
	 * Loads a single key via the loader
	 *
	 * @param K $key
	 */
	public function loadOne($key) {
		try {
			$loader = $this->loader;
			$loaded = $loader($key);
			$this->cache($key, $loaded);
		} catch (Exception $exception) {
			Log::error($exception->getMessage());
			throw $exception;
		}
	}


	/**
	 * @psalm-param K $key
	 * @psalm-return V
	 */
	public function get($key) {
		if (!array_key_exists($key, $this->objects)) {
			$this->loadOne($key);
		}
		return parent::get($key);
	}

}