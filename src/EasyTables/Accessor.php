<?php


namespace EZCake\EasyTables;


use Cake\Controller\Component;
use Cake\Core\InstanceConfigTrait;
use Cake\ORM\Query;

class Accessor extends Component {

	use InstanceConfigTrait;

	/**
	 * @param string|string[] $fields
	 */
	public function allowSort($fields) {
		if (!is_array($fields)) {
			$fields = [$fields];
		}
		$this->setConfig('fields.sort', $fields, true);
	}

	/**
	 * @param string|string[] $fields
	 */
	public function allowSearch($fields) {

	}

	public function buildQuery(Query $query) {

		/**
		 * @todo
		 */
		$conditions = [];
		foreach ($this->getSearchQuery() as $field => $value) {
			$stringParts = explode(" ", $value);
			$queryParts = [];
			foreach ($stringParts as $stringPart) {
				$queryParts[] = ["$field LIKE" => "%$value%"];
			}
		}
	}

	public function getSearchQuery(): array {

	}

	public function allowAccess(Query $query) {

		$request = $this->getController()->getRequest();


	}


}