<?php
/**
 * Created by PhpStorm.
 * User: lordphnx
 * Date: 12/10/17
 * Time: 1:20 PM
 */

namespace EZCake\EasyTest;


use Cake\Datasource\ConnectionInterface;
use Cake\TestSuite\Fixture\TestFixture;

/**
 * Class PersistentFixture
 *
 * This fixture assumes that the test schema is created once, before fixture calling
 * Fixtures should empty the tables, but not recreate them
 *
 */
class PersistentFixture extends TestFixture {

    public function drop(ConnectionInterface $db): bool {
		return false;
	}

    /**
     * Do not truncate at the end of tests
     * @param ConnectionInterface $db
     * @return bool
     */
	public function truncate(ConnectionInterface $db): bool {
		return false;
	}

	private function _doTruncate(ConnectionInterface $db): bool {
		return $db->execute("DELETE FROM {$this->table} WHERE 1=1")->execute();
	}
	
    /**
     * Truncate rather than create the table
     * @param ConnectionInterface $db
     * @return bool
     */
	public function create(ConnectionInterface $db): bool {
		return $this->_doTruncate($db);
	}

    /**
     * To be sure to avoid duplicate entries, trucate before inserting
     * @param ConnectionInterface $db
     * @return bool
     */
	public function insert(ConnectionInterface $db) {
		$this->_doTruncate($db);
		return parent::insert($db);
	}


}