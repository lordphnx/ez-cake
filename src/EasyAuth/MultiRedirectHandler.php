<?php


namespace EZCake\EasyAuth;


use Authorization\Exception\Exception;
use Authorization\Middleware\UnauthorizedHandler\RedirectHandler;
use Cake\Http\Response;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

class MultiRedirectHandler extends RedirectHandler {


	public function handle(Exception $exception, ServerRequestInterface $request, array $options = []): ResponseInterface {


		$url = $this->getUrlForException($exception, $options);

		$response = new Response();
		
		$options['url'] = $url;
		
		$url = $this->getUrl($request, $options);
		
		return $response
			->withHeader('Location', $url)
			->withStatus(302);
	}


	public function getUrlForException(Exception $exception, array $options) {
		foreach ($options['exceptions'] as $class => $url) {
			if ($exception instanceof $class) {
				return $url;
			}
		}
		throw $exception;
	}

}