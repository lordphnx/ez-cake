<?php


namespace EZCake\EasyAuth;


use Authorization\IdentityInterface;
use Authorization\Policy\RequestPolicyInterface;
use Cake\Core\App;
use Cake\Http\ServerRequest;
use Cake\Log\Log;

class PerControllerPolicy implements RequestPolicyInterface {
	
	
	public function delegateAuthToControler(?IdentityInterface $identity, ServerRequest $request): bool {
		$plugin = $request->getParam('plugin');
		$controller = $request->getParam('controller');


		$class = $controller;
		if (!empty($plugin)) {
			$class = $plugin . '.' . $class;
		}

		$policyClassName = App::className($class, 'Policy', 'Policy');
		if ($policyClassName === null) {
			//@todo: return defaultResult()
			Log::error("Trying to get {$policyClassName} but no such policy found");
			return false;
		}

		/** @var RequestPolicyInterface $policy */
		$policy = new $policyClassName();
		return $policy->delegateAuthToControler($identity, $request);

		//@todo: see if there is a more specific Policy
		//default response for this policy
		return false;
	}
}