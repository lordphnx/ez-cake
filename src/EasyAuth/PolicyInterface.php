<?php


namespace EZCake\EasyAuth;


use Authorization\IdentityInterface;
use Authorization\Policy\RequestPolicyInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * Interface PolicyInterface
 * @package EZCake\EasyAuth
 * @deprecated use {@link RequestPolicyInterface} instead
 */
interface PolicyInterface {
	public function canAccess(?IdentityInterface $identity, ServerRequestInterface $resource): bool;
}