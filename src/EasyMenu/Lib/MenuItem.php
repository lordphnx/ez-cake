<?php

namespace EZCake\EasyMenu\Lib;

class MenuItem {
	public $label;
	public $icon;
	public $url;

	public static function create($label = null): MenuItem {
		$instance = new self();
		if ($label !== null) {
			$instance->withLabel($label);
		}
		return $instance;
	}

	public function withUrl($url): MenuItem {
		$this->url = $url;
		return $this;
	}

	public function withIcon($icon): MenuItem {
		$this->icon = $icon;
		return $this;
	}

	public function withLabel($label): MenuItem {
		$this->label = $label;
		return $this;
	}


}