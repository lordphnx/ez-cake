<?php
declare(strict_types=1);

namespace EZCake\EasyNav\Helper;

use Cake\Core\App;
use Cake\Log\Log;
use Cake\Utility\Inflector;
use Cake\View\Helper;
use Cake\View\Helper\BreadcrumbsHelper;
use OneBoard\Nav\NavSpec;
use function is_string;

/**
 * Breadcrumbs helper
 * @property Helper\HtmlHelper $Html
 * @property BreadcrumbsHelper $Breadcrumbs
 */
class NavHelper extends Helper {


    protected $helpers = ['Html', 'Breadcrumbs'];


    /**
     * Reset the helper for start in a new breadcrumb
     */
    public function reset(): void {
        $this->Breadcrumbs->reset();
    }


    /**
     * Gets the pass part of the Url
     * @param array $url
     * @return array
     */
    private static function getPass(array $url): array {
        $keys = array_keys($url);
        foreach ($keys as $key) {
            if (is_string($key)) {
                unset($url[$key]);
            }
        }
        return $url;
    }

    /**
     * @return array the current URL in array form
     */
    public function getCurrentUrl(): array {
        $currentUrl = [
            'plugin' => $this->getView()->getRequest()->getParam('plugin'),
            'controller' => $this->getView()->getRequest()->getParam('controller'),
            'action' => $this->getView()->getRequest()->getParam('action', 'index') ?? 'index',
        ];
        $currentUrl += $this->getView()->getRequest()->getParam('pass', []);
        return $currentUrl;
    }

    /**
     * @return string
     * Magically render a breadcrumb trail for the given entity
     */
    public function magicBreadcrumbs(): string {
        $currentUrl = $this->getCurrentUrl();
        $first = true;
        do {
            //peel the current Url into its parts
            $plugin = $currentUrl['plugin'] ?? null;
            $controller = $currentUrl['controller'];
            $action = $currentUrl['action'];
            $pass = self::getPass($currentUrl);

            //get the correct NavSpec
            $spec = $this->getNav($plugin, $controller, $action, $pass);
            if ($spec === null) {
                if ($first) {
                    return 'There is no Nav class for ' . print_r($currentUrl, true);
                }
                break;
            }

            //Add the breadcrumb, make it a "blank" in case it's the first
            $this->Breadcrumbs->prepend($spec->getTitle(), $first ? null : $currentUrl);

            //prep the next cycle
            $first = false;
            $currentUrl = $spec->getParentUrl();
        } while ($currentUrl !== null);

        //render the breadcrumb trail
        return $this->Breadcrumbs->render();
    }

    /**
     * @param string|null $plugin
     * @param string $controller
     * @return string|null The namespaced classname for the nav for the given plugin and controller
     */
    public static function resolveClass(?string $plugin, string $controller): ?string {
        if ($plugin !== null) {
            $plugin = Inflector::camelize($plugin);
        }

        $controller = Inflector::camelize($controller);
        $class = $controller;

        if (!empty($plugin)) {
            $class = "$plugin.$controller";
        }
        $class = App::className($class, 'Nav', 'Nav');
        if ($class === null) {
            Log::error("Could not make a namespaced classpath for Nav: $class");
        }
        return $class;
    }

    /**
     * @param ?string $plugin
     * @param string $controller
     * @param string $action
     * @param array $pass
     * @return NavSpec|null
     */
    public function getNav(?string $plugin, string $controller, string $action, array $pass = []): ?NavSpec {
        //determine the nav class
        $navClass = static::resolveClass($plugin, $controller);
        if ($navClass === null) {
            return null;
        }

        //create an instance of the NavClass
        if (!class_exists($navClass)) {
            Log::error("NavClass $navClass does not exist");
            return null;
        }
        $navObj = new $navClass();

        if (!method_exists($navObj, $action)) {
            Log::error("Method $action does not exist on $navClass");
            return null;
        }
        return $navObj->$action(...$pass);
    }

}
