Say you have these controllers in `src/Controllers/`

```php
class UsersController extends AppController{
    public function view($user_id) {
        //..
    }
}

class PostsController extends AppController{
    public function view($post_id) {
        //..    
    }
}
```

Create `src/Nav`

```php

class PostsNav {
    public function view($post_id) : NavSpec {
        
        //add custom logic such as ORM/DB logic
        $post = $this->Posts->get($post_id);
        
        return  new NavSpec(
            //This will become the title of this page in your breadcrumbs
            "View post #{$post_id}",
            //This is the CakePHP URL Spec of the parent page, or null if there is none
            [
                'plugin' => null,
                'controller' => 'Users',
                'action' => 'view',
                $post->user_id
            ])    
    } 
}

```