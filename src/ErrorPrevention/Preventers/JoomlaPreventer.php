<?php


namespace  EZCake\ErrorPrevention\Preventers;


class JoomlaPreventer extends UrlRegexPreventer {

	public static $blockPath = [
		'/joomla/i'
	];
}