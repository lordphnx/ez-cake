<?php


namespace EZCake\ErrorPrevention\Preventers;

/**
 * Blocks exceptions related to wordpress
 * @package ErrorPrevention\Preventers
 */
class WordpressPreventer extends UrlRegexPreventer {

	public static $blockPath = [
		'/wordpress/i',
		'/wp-/i',
		'/wpindex/i',
		'/[.]xmlrpc[.]txt/i',
		'/[.]xmlrpc.php/i',
		'/xmlrpc/i',
	];
	

}