<?php

namespace EZCake\ErrorPrevention\Preventers;

use Psr\Http\Message\ServerRequestInterface;
use Throwable;

class DefaultPreventer implements PreventerInterface {

	/**
	 * @var PreventerInterface[] 
	 */
	private $preventers;
	
	public function __construct() {
		$this->preventers = [
			new ForeignExtensionPreventer(),
			new WordpressPreventer(),
			new JoomlaPreventer(),
			new MagnetoPreventer(),
			new SecretFilePreventer(),
			
		];
	}


	public function shouldBlock(ServerRequestInterface $request) :bool{
		foreach ($this->preventers as $preventer) {
			if ($preventer->shouldBlock($request)) {
				return true;
			}
		}
		return false;
	}

	public function shouldSkipReport(ServerRequestInterface $request, Throwable $throwable):bool {
		foreach ($this->preventers as $preventer) {
			if ($preventer->shouldSkipReport($request, $throwable)) {
				return true;
			}
		}
		return false;
	}


}