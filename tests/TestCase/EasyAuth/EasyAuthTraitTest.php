<?php

namespace EZCake\Test\TestCase\EasyAuth;

use Cake\Datasource\EntityInterface;
use Cake\Http\ServerRequest;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;
use EZCake\EasyAuth\EasyAuthTrait;
use EZCake\EasyAuth\error\MissingParameterException;
use PHPUnit\Framework\MockObject\MockObject;

/**
 * @property Table|MockObject $tableMock
 */
class EasyAuthTraitTest extends TestCase {

	use EasyAuthTrait;

	protected function setUp(): void {
		parent::setUp();
		$this->tableMock = $this->createMock(Table::class);
		TableRegistry::getTableLocator()->set('Users', $this->tableMock);
	}

	protected function tearDown(): void {
		parent::tearDown();
		TableRegistry::getTableLocator()->remove('Users');
	}

	public function testHappyFlowGet() {
		$request = new ServerRequest([
			'params' => [
				'pass' => [0 => 123]
			]
		]);

		$this->tableMock->expects($this->once())->method('get')->with(123);
		$this->IDArgAuth($request, [0 => 'Users'], static function (EntityInterface $entity) {

		});
	}

	/**
	 * https://gitlab.com/lordphnx/ez-cake/-/issues/9
	 * @return void
	 */
	public function testIssue9Get() {
		$request = new ServerRequest([
			'params' => [
				'pass' => []
			]
		]);
		$this->expectException(MissingParameterException::class);
		$this->IDArgAuth($request, [0 => 'Users'], static function (EntityInterface $entity) {

		});
	}

	/**
	 * https://gitlab.com/lordphnx/ez-cake/-/issues/9
	 * @return void
	 */
	public function testIssue9Post() {
		$request = new ServerRequest([
			'post' => [

			]
		]);


		$this->tableMock->expects($this->once())->method('get')->with(null);
		$this->PostArgAuth($request, ['me' => 'Users'], static function (EntityInterface $entity) {

		});

	}

}