<?php

namespace EZCake\Test\TestCase\ErrorPrevention;

use Cake\Http\ServerRequest;
use Cake\TestSuite\TestCase;
use EZCake\ErrorPrevention\Preventers\ForeignExtensionPreventer;

/**
 * @property ForeignExtensionPreventer $subject
 */
class ForeignExtensionPreventerTest extends TestCase {

	protected function setUp(): void {
		parent::setUp();
		$this->subject = new ForeignExtensionPreventer();
	}

	public function testBlock() {
		$request = new ServerRequest([
			'url' => 'https://localhost:8765/test.php'
		]);
		self::assertTrue($this->subject->shouldBlock($request));
	}

	public function testAllow() {
		$request = new ServerRequest([
			'url' => 'https://localhost:8765/testphp'
		]);
		self::assertFalse($this->subject->shouldBlock($request));
	}

}